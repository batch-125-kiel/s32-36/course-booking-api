const User = require("./../models/User");
const bcrypt = require('bcrypt');
const auth = require('./../auth');
//require course model db
const Course = require('./../models/Courses');


module.exports.checkEmailExists = (reqBody) =>{

	return User.find({email: reqBody.email}).then((result, error) =>{

		if(result.length != 0){

			return true
		}else{

			return false
		}
	})


}


module.exports.register = (reqBody) =>{

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName:  reqBody.lastName,
		email: 	   reqBody.email,
		mobileNo:  reqBody.mobileNo,
		password:  bcrypt.hashSync(reqBody.password, 10)
		//password:  reqBody.password
		
	});

	return newUser.save().then((result, error)=>{

		if (error){
			return error;
		}else{

			return true;
		}
	})

}

module.exports.login = (reqBody) =>{
	//model method

	return User.findOne({email: reqBody.email}).then((result, error)=>{

			if(result == null){

				return false;

			}else{

				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				if (isPasswordCorrect === true){

					return {access: auth.createAccessToken(result.toObject())};

				}else{

					return false;
				}

			}

	})


}

module.exports.getProfile = (data) => {

	return User.findById(data).then(result =>{
		result.password = "**********"
		return result;
	})
}
//async google this
module.exports.enroll = async(data) =>{

//save user enrollments
const userSaveStatus = await User.findById(data.userId).then(user =>{

//enrollments array
	user.enrollments.push({courseId: data.courseId})

	return user.save().then((user, error)=>{

		if(error){

			return false;
		}else{

			return true;
		}

	})
})
//save course enrollees
//try with no async and await
const courseSaveStatus = await Course.findById(data.courseId).then(course =>{

	//push data into the array first
	course.enrollees.push({userId:data.userId});


	//save the new array
	return course.save().then((course, error)=>{

		if(error){

			return false;
		}else{

			console.log(`COURSE ENROLLEE SAVED ${course}`);

			return true;
		}


	})
})



if(userSaveStatus && courseSaveStatus){

	return true;
}else{

	return false;
}
}

module.exports.editProfile =(id, reqBody)=>{

	return User.findByIdAndUpdate(id,reqBody).then((result, error)=>{

		return error ? false : true;

	});




}

const Course = require('./../models/Courses');

module.exports.addCourse = (reqBody)=>{

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	});

	return newCourse.save().then((result, error)=>{

		if (error){

			return error;
		}else{

			return true;
		}

	})

}

module.exports.getAllcourses = (id)=>{

//made getallcourses() also a function for user enrolled courses
//if id is undefined, function is for list all courses, else list users enrolled courses

		if(id !== undefined){

			//console.log('THIS IS ENROLLED COURSES ROUTE');
			return Course.find({"enrollees.userId": id}).sort({"enrollees.enrolledOn": -1}).then((result, error)=>{

				if(error){

					return error;

				}else{

					return result;

				}

			})


		}else{

			//console.log('THIS IS VIEW ALL COURSES');
			return Course.find().sort({"createdOn": -1}).then((result, error)=>{

				return result;

	})

}


}
module.exports.getAllActive = ()=>{

	return Course.find({isActive: true}).then(result => result);

}

module.exports.getSingleCourse = (id)=>{

	return Course.findById(id).then((result, error) =>{

		return error ? error : result;


	});
}
module.exports.editCourse = (id,reqBody)=>{


	return Course.findByIdAndUpdate(id,reqBody).then((result, error)=>{

		return error ? error : result;

	});
}

module.exports.archiveCourse = (id)=>{

	return Course.findByIdAndUpdate(id, {isActive: false}, {new: true}).then((result, error)=>{

		return error ? false : true;
	})

}

module.exports.unArchiveCourse = (id)=>{

	return Course.findOneAndUpdate({_id:id}, {isActive: true}).then((result, error)=>{

		return error ? false : true;
	})

}
module.exports.deleteCourse = (id)=>{

	return Course.deleteOne({_id:id}).then((result, error)=>{

		return error ? false : true;
	})
}
const express = require('express');
const router = express.Router();
const courseController = require('./../controllers/courseControllers');
const auth = require('./../auth');
const path = require('path');

//retrieve all active courses

router.get('/active', (req, res)=>{

	courseController.getAllActive().then(result => res.send(result))

})

router.post('/add', auth.verify, (req, res)=>{

	courseController.addCourse(req.body).then(result => res.send(result))

})
//retrieve all courses
router.get('/all/', auth.verify, (req, res)=>{

	courseController.getAllcourses().then(result =>res.send(result));

})

router.get('/enrollments/:id', auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.getAllcourses(id).then(result =>res.send(result));


})

router.get('/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.getSingleCourse(id).then(result => res.send(result));


})

router.put('/edit/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.editCourse(id, req.body).then(result => res.send(result));

});

router.put('/archive/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.archiveCourse(id).then(result => res.send(result));

})

router.put('/unarchive/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.unArchiveCourse(id).then(result => res.send(result));

})

router.delete('/delete/:id',auth.verify, (req, res)=>{

	let id = req.params.id;
	courseController.deleteCourse(id).then(result => res.send(result));

})

module.exports = router;

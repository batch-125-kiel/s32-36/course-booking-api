const express = require('express');
const mongoose = require('mongoose');
const PORT =process.env.PORT ||  3000;
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');//google this

let userRoutes = require("./routes/userRoutes");
let courseRoutes = require("./routes/courseRoutes");

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://kielpaulo:damienzk@cluster0.y6fy9.mongodb.net/course_booking?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true})
.then(()=>console.log('Connected to the database'))
.catch((error)=> console.log(error));

//ROUTES
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

app.listen(PORT, ()=>console.log(`Running in port ${PORT}`));

//SCHEMA


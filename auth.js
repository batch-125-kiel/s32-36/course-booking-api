//we will use this for authentication
	//for login
	//retrieve user details

//Json Web Token
	//3 methods
		//sign(data, secret, {options})- creates the token
		//verify(token, secret, callback func()) - checks if the token is present
		//decode(token, {options}.payload) - interpret/decrypts the token 


let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";


//create token
module.exports.createAccessToken = (user)=>{

	const data = {

		id: user._id,
		email:user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next)=>{

	let token = req.headers.authorization;

	//console.log(token);

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
		//console.log(token);

		return jwt.verify(token, secret, (error, data)=>{

			if(error){

				
				return res.send({auth: "failed"});

			}else{

				next();
				
			}
		});


	}

}

//decode token

module.exports.decode = (token)=>{

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
		
		return jwt.verify(token, secret, (error, data)=>{

			if(error){

				return null;

			}else{


			return jwt.decode(token, {complete: true}).payload;


				//next();
				//return data;
				console.log(data);
			}
		});


	}

}

//decode(req.headers.authorization);





/*eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxMzBhYzc5MjQyOWNjM2IxZTRjZmZkMSIsImVtYWlsIjoiYW5hQHNvcGhpYS5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjMwNTgwMTkwfQ.04tetmSDiZx_Nn0mU6h40pdVegOcqZryG2ktl1vePiY*/